#include <iostream>
#include <iomanip>
#include "ftf.h"
#include <math.h>
#include <time.h>
#include <stdlib.h> 

using namespace std;
# define PI          3.1415
double sampleNormal() {
    double u = ((double) rand() / (RAND_MAX)) * 2 - 1;
    double v = ((double) rand() / (RAND_MAX)) * 2 - 1;
    double r = u * u + v * v;
    if (r == 0 || r > 1) return sampleNormal();
    double c = sqrt(-2 * log(r) / r);
    return u * c;
}

int main()
{
    double i = 1;
    double x, d, p;
    FTF ftf;
    clock_t tstart, tend;

    while (i<=10000) {
        tstart = clock();
        x = sin(2*100*PI*i);
        p = sin(2*110*PI*i+0.2)+(double)sin(2*90*PI*i-0.2);
        d = 100*x+p+ 0.05*sampleNormal();
        ftf.process(x,d);
        tend = clock()-tstart;
        cout<<setprecision(8)<<x<<" ";
        cout<<setprecision(8)<<p<<" ";
        cout<<setprecision(8)<<d<<" ";
        cout<<setprecision(8)<<ftf.getdata()-p<<" ";
        cout<<setprecision(8)<<((float)tend*1000/CLOCKS_PER_SEC)<< endl;
        i++;
    }

    return 0;
}

