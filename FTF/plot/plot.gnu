set terminal epslatex #size 3.5,2.62 color colortext
set output 'results_TFT1.tex'

set tmargin 2.5
set bmargin 2.5
set lmargin 2.5
set rmargin 2.5

set multiplot layout 2, 1 title "Results of FTF" #font ",14"

set title "signals"
set ylabel "Amplitude"
set xlabel "Iterations"
set yr [-2:2]
set grid
unset key
set tics scale 0
plot "data.txt" using 1 with line lt -1 lw 1 lc rgb "red"

set title "echoes"
set ylabel "Amplitude"
set xlabel "Iterations"
set yr [-3:3]
set grid
set tics scale 0
plot "data.txt" using 2 with line lt -1 lw 1 lc rgb "green"

unset multiplot
set output 'results_TFT2.tex'
set multiplot layout 2, 1 title "Results of FTF" #font ",14"

set title "signals + echoes"
set ylabel "Amplitude"
set xlabel "Iterations"
set yr [-140:140]
set grid
set tics scale 0
plot "data.txt" using 3 with line lt -1 lw 1 lc rgb "blue"

set title "Error"
set ylabel "Amplitude"
set xlabel "Iterations"
set grid
set tics scale 0
set yr [-5:5]
plot "data.txt" using 4 with line lt -1 lw 1 lc rgb "black"

#unset multiplot
