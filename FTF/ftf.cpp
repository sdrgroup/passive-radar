#include "ftf.h"
#include <math.h>
#include <stdio.h>
#include <iostream>
using namespace std;
FTF::FTF()
{
    gamma = 1;
    newvec = new double[N+1];
    phi = new double[N];
    phi1 = new double[N+1];
    uvec = new double[N];
    w = new double[N];
    wb = new double[N];
    wf = new double[N];
    tmp = new double[N+1];
    for(unsigned int i=0; i<N; i++)
    {
        phi[i]=0;
        uvec[i]=0;
        w[i]=0;
        wb[i]=0;
        wf[i]=0;

    }
    e=0;
    epf = epsilon;
    epb = epsilon;
}

int FTF::process(double x, double d)
{
    unsigned int i;
    double *pointer_tmp;
    //Forward a Priori Prediction Error
    efa = x;
    for(i=0; i<N; i++)
        efa+=-uvec[i]*wf[i];
    //Forward a Posterior Prediction Error
    ef = efa*gamma;
    phi1[0] = efa/(lambda*epf);
    for(i=0; i<N; i++)
        phi1[i+1]=phi[i]-efa*wf[i]/(lambda*epf);

    //Forward a Weight Update
    for(i=0; i<N; i++)
        wf[i]+=ef*phi[i];
    //M+1 Converser Factor
    gamma1 = gamma*lambda*epf;
    //MWLS Forward Error
    epf = lambda*epf + efa*ef;
    //M+1 Converser Factor
    gamma1 /=epf;

    //Backward a Priori Prediction Error
    eba = lambda*epb*phi1[N];
    //M Converser Factor
    gammainv = 1/gamma1-phi1[N]*eba;
    gamma = 1/gammainv;
    //Backward a Posteriori Prediction Error
    eb = eba*gamma;
    //MWLS Backward Error
    epb = lambda*epb+eb*eba;
    //M Converser Weight
    for(i=0; i<N; i++)
        phi[i] = phi1[i]+phi1[N]*wb[i];
    //Backward a Weight Update
    for(i=0; i<N; i++)
        wb[i]+=eb*phi[i];
    //Update with New Sample of Input Data
    for(i=1; i<N; i++)
        tmp[i]=uvec[i-1];
    pointer_tmp = uvec;
    uvec = tmp;
    tmp = pointer_tmp;
    uvec[0]=x;
    //A priori Joint-Estimation Error
    ea = d;
    for(i=0; i<N; i++)
        ea+=-uvec[i]*w[i];
    //A Posteriori Joint-Estimation Error
    e = ea*gamma;
    //Joint-Estimation Weight Update
    for(i=0; i<N; i++)
       w[i]+=+e*phi[i];
    return 0;
}

double FTF::getdata()
{
    return e;
}
