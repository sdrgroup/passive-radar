#ifndef FTF_H
#define FTF_H

#define N 10

#define lambda 1
#define epsilon 0.00001
class FTF
{
private:
    double ea;
    double eb;
    double eba;
    double ef;
    double efa;
    double epb;
    double epf;
    double gamma;
    double gamma1;
    double gammainv;
    double e;
    double *newvec;
    double *phi;
    double *phi1;
    double *uvec;
    double *w;
    double *wb;
    double *wf;
    double *tmp;

public:
    FTF();
    int process(double x, double d);
    double getdata();
};

#endif // FTF_H
