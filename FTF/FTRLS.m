%Noise Generation
clc
%clear all
close all
M = 10; N = 1000; Delay = 3; lambda = 1;
signal = sin(2*pi*100*linspace(0,1,N)');
ref=sin(2*pi*115*(linspace(0,1,N)'-2))+sin(2*pi*85*(linspace(0,1,N)'+2));
d = 100*signal+ref;

%initialization
epsilon = 0.00001;
w = zeros(M, N);
uvec = zeros(1, M);
e = zeros(N, 1);
wf(1:M, 1) = 0;
wb(1:M, 1) = 0;
w(1:M, 1) = 0;
phi(1:M, 1) = 0;
gamma(1) = 1;
epb(1:M, 1) = epsilon;
epf(1:M, 1) = epsilon;
%Filtering Algorithm
measure = zeros(1,N);
for i=2:length(signal)
	id = tic ();
	%Forward a Priori Prediction Error
	efa(i) = signal(i)-uvec*wf(:, i-1);
	%Forward a Posterior Prediction Error
	ef(i) = efa(i)*gamma(i-1);
	%MWLS Forward Error
	epf(i) = lambda*epf(i-1)+efa(i)*ef(i);
	%Forward a Weight Update
	wf(:, i) = wf(:, i-1)+phi(:, i-1)*ef(i);
	phi1(:, i) = [0; phi(:, i-1)] + (efa(i)/(lambda*epf(i-1))*[1; -wf(:, i-1)]);	
	%M+1 Converser Factor
	gamma1(i) = gamma(i-1)*lambda*epf(i-1)/epf(i);
	%Backward a Priori Prediction Error
	eba(i) = lambda*epb(i-1)*phi1(M+1, i);
	%M Converser Factor
	gammainv = 1/gamma1(i)-phi1(M+1, i)*eba(i);
	gamma(i) = 1/gammainv;
	%Backward a Posteriori Prediction Error
	eb(i) = eba(i)*gamma(i);
	%MWLS Backward Error
	epb(i) = lambda*epb(i-1)+eb(i)*eba(i)';
	%M Converser Weight
	newvec = phi1(:, i)-phi1(M+1, i)*[-wb(:, i-1); 1];
	phi(:, i) = newvec(1:M);
	%Backward a Weight Update
	wb(:, i) = wb(:, i-1)+phi(:,i)*eb(i);
	%Update with New Sample of Input Data
	uvec = [signal(i) uvec(1:M-1)];
	%A priori Joint-Estimation Error
	ea(i) = d(i)-uvec*w(:, i-1);
	%A Posteriori Joint-Estimation Error
	e(i) = ea(i)*gamma(i);
	%Joint-Estimation Weight Update
	w(:,i) = w(:,i-1) +e(i)*phi(:,i);
	measure(i)=toc (id);
end
subplot(4,1,1)
plot(d)
subplot(4,1,2)
plot(signal)
subplot(4,1,3)
plot(e)
subplot(4,1,4)
plot(ref)

figure
plot(20*log10(abs(e-ref)))
