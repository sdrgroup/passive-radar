#ifndef PROCESSING_H
#define PROCESSING_H

#include <fftw3.h>
#include <QThread>

#define N 400
#define Nf 200

struct Matrix
{
    float data[Nf*N];
};

class Processing : public QThread
{
    Q_OBJECT
public:
    Processing();
private:
    fftw_plan ps, pr, px;
    fftw_complex *sx, *rx;
    fftw_complex *sx_ext, *rx_ext;
    fftw_complex *Sx, *Rx;
    fftw_complex *out, *result;
    double *output;
   Matrix xcorrData;
protected:
    void run();
signals:
    void newData(const Matrix &);
public slots:
};

#endif // PROCESSING_H
