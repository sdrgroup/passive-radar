#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    setWindowTitle("Passive Radar");
    setup();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(100);
    date = new QDateTime();

    controller = new Controller();
    connect(controller->processing, SIGNAL(newData(Matrix)), this, SLOT(update(Matrix)),Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setup()
{
    ui->plot->axisRect()->setupFullAxesBox(true);

    ui->plot->plotLayout()->insertRow(0);
    title = new QCPPlotTitle(ui->plot, date->currentDateTime().toString());
    ui->plot->plotLayout()->addElement(0, 0, title);

    ui->plot->xAxis->setLabel("Doppler (Hz)");
    ui->plot->yAxis->setLabel("Range (Km)");

    // set up the QCPColorMap:
    colorMap = new QCPColorMap(ui->plot->xAxis, ui->plot->yAxis);
    ui->plot->addPlottable(colorMap);

    colorMap->data()->setSize(Nf, N); // we want the color map to have nx * ny data points
    colorMap->data()->setRange(QCPRange(-4, 4), QCPRange(-4, 4)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions


    // now we assign some data, by accessing the QCPColorMapData instance of the color map:


    // add a color scale:
    QCPColorScale *colorScale = new QCPColorScale(ui->plot);
    ui->plot->plotLayout()->addElement(1, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorMap->setColorScale(colorScale); // associate the color map with the color scale

    // set the color gradient of the color map to one of the presets:
    colorMap->setGradient(QCPColorGradient::gpJet);
    // we could have also created a QCPColorGradient instance and added own colors to
    // the gradient, see the documentation of QCPColorGradient for what's possible.

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    colorMap->rescaleDataRange();

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroup = new QCPMarginGroup(ui->plot);
    ui->plot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    // rescale the key (x) and value (y) axes so the whole color map is visible:
    ui->plot->rescaleAxes();
}


void MainWindow::update(Matrix data)
{
    for (int xIndex=0; xIndex<Nf; ++xIndex)
    {
      for (int yIndex=0; yIndex<N; ++yIndex)
      {
            colorMap->data()->setCell(xIndex, yIndex, data.data[xIndex*N+yIndex]);
      }
    }
    ui->plot->replot();
    title->setText(date->currentDateTime().toString());
    colorMap->rescaleDataRange();
    ui->plot->rescaleAxes();
}
