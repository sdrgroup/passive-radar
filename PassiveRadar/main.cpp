#include "mainwindow.h"
#include <QApplication>
#include <fftw3.h>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType < Matrix > ("Matrix");
    MainWindow w;
    w.show();
    return a.exec();
}
