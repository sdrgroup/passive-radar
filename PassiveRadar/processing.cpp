#include "processing.h"
#include <fftw3.h>
#include <math.h>
#include<complex.h>

#include <QDebug>

#define Re 0
#define Im 1
Processing::Processing()
{
    output = (double *) malloc(sizeof(double)*(2*N-1));
    sx = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);
    rx = (fftw_complex *) fftw_malloc(sizeof(fftw_complex)*N);

    sx_ext = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (2 * N - 1));
    rx_ext = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (2 * N - 1));

    Sx = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (2 * N - 1));
    Rx = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (2 * N - 1));
    out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (2 * N - 1));
    result = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * (2 * N - 1));


    ps = fftw_plan_dft_1d(2 * N - 1, sx_ext, Sx, FFTW_FORWARD, FFTW_ESTIMATE);
    pr = fftw_plan_dft_1d(2 * N - 1, rx_ext, Rx, FFTW_FORWARD, FFTW_ESTIMATE);
    px = fftw_plan_dft_1d(2 * N - 1, out, result, FFTW_BACKWARD, FFTW_ESTIMATE);

    for (int i=0; i<N; i++)
    {
        sx[i][Re]= sin(-0.1*i);
        sx[i][Im]= sin(-0.1*i);
        rx[i][Re]= cos(0.1*i);
        rx[i][Im]= cos(-0.1*i);
    }

}


void Processing::run()
{
    while(true)
    {
        memset (sx_ext, 0, sizeof(fftw_complex) * (N - 1));
        memset (rx_ext + N, 0, sizeof(fftw_complex) * (N - 1));
        memcpy (sx_ext + (N - 1), sx, sizeof(fftw_complex) * N);
        memcpy (rx_ext, rx, sizeof(fftw_complex) * N);

        for(int i=0; i<Nf; ++i)
        {
            fftw_execute(ps);
            fftw_execute(pr);
            for (int p = 0; p < 2 * N - 1; p++)
            {
                out[i][Re] = (Sx[p][Re]*Rx[p][Re]+Sx[p][Im]*Rx[p][Im])/((2*N-1)*(2*N-1));
                out[i][Im] = (Sx[p][Re]*Rx[p][Im]-Sx[p][Im]*Rx[p][Re])/((2*N-1)*(2*N-1));
            }
            fftw_execute(px);
            for (int p = 0; p < 2 * N - 1; p++)
            {
                output[p]= sqrt(result[p][Re]*result[p][Re]+result[p][Im]*result[p][Im]);
                //qDebug()<<output[p];
            }
            //i = 0 ----> 0
            //i = 1 ----> N
            //
            memcpy(&xcorrData.data[i*N], output, N);
            fftw_cleanup();
        }
        emit newData(xcorrData);
//QThread::msleep(1000);
    }
}
