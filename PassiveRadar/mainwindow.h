#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QDateTime>
#include "qcustomplot.h"
#include "controller.h"
#include <QVector2D>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setup();
private:
    Controller *controller;
    Ui::MainWindow *ui;
    QCPPlotTitle *title;
    QTimer *timer;
    QCPColorMap *colorMap;
    QDateTime *date;
public slots:
    void update(Matrix data);
};

#endif // MAINWINDOW_H
