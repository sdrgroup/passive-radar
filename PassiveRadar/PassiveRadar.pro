#-------------------------------------------------
#
# Project created by QtCreator 2015-09-29T04:53:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = PassiveRadar
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    controller.cpp \
    processing.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    controller.h \
    processing.h

FORMS    += mainwindow.ui

LIBS += -lfftw3
