#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "processing.h"

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = 0);
    Processing *processing;
private:

signals:

public slots:
};

#endif // CONTROLLER_H
